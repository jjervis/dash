#!/bin/bash

sudo apt-get clean
sudo apt-get autoremove -y
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install xdotool unclutter sed -y
sudo raspi-config nonint do_ssh 0
sudo raspi-config nonint do_wifi_country CA


sudo wget https://bitbucket.org/jjervis/dash/raw/master/kiosk.sh -O /home/pi/kiosk.sh
sudo wget https://bitbucket.org/jjervis/dash/raw/master/kiosk.service -O /lib/systemd/system/kiosk.service
sudo chmod +x /home/pi/kiosk.sh

sudo systemctl enable kiosk.service
sudo systemctl start kiosk.service

sudo raspi-config nonint do_boot_behaviour B4